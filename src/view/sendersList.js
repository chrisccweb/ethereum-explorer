import React, { Component } from 'react'

class SendersList extends Component {
    render() { 
        const {
            senders,
        } = this.props;

        let count = 0;

        return ( 
            <div>
            <h3>Senders</h3>
            <table>
                <thead>
                <tr>
                    <th></th>
                    <th>Senders ({Object.keys(senders).length} unique)</th>
                    <th>Total Ether</th>
                </tr>
                </thead>
                <tbody>
                {Object.entries(senders).map(([address, ether]) => {
                return <tr key={count++}>
                            <td>{count}</td>
                            <td>{address}</td>
                            <td>{ether/1000000000000000000}</td>
                       </tr>;
                })}
                </tbody>
            </table>
            </div>
        );
    }
}
 
export default SendersList;