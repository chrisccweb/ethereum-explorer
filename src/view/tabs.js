import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Summary from './summary';
import SendersList from './sendersList';
import ReceiversList from './receiversList';
import EventsList from './eventsList';
import ContractsList from './contractsList';

function TabContainer(props) {
  return (
    <Typography component="div" style={{ padding: 8 * 3 }}>
      {props.children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
};

const styles = theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
});

class SimpleTabs extends React.Component {
  state = {
    value: 0,
  };

  handleChange = (event, value) => {
    this.setState({ value });
  };

  render() {
    const {
      classes,
      newContracts,
      receivers,
      senders,
      events,
      existingContracts,
      blockHeight,
      totalETHTransferred,
      uncles,
      contractTransactionsPercentage,
      totalEvents,
    } = this.props;
    const { value } = this.state;

    return (
      <div className={classes.root}>
        <AppBar position="static">
          <Tabs value={value} onChange={this.handleChange}>
            <Tab label="Summary" />
            <Tab label="Senders" />
            <Tab label="Receivers" />
            <Tab label="Contracts" />
            <Tab label="Event" />
          </Tabs>
        </AppBar>
        {value === 0 &&
          <TabContainer>
            <Summary
              blockHeight={blockHeight}
              totalETHTransferred={totalETHTransferred}
              uncles={uncles}
              contractTransactionsPercentage={contractTransactionsPercentage}
            />
          </TabContainer>}
        {value === 1 && 
          <TabContainer>
            <SendersList
              senders={senders}
            />
          </TabContainer>}
        {value === 2 && 
          <TabContainer>
            <ReceiversList
              receivers={receivers}
              newContracts={newContracts}
              existingContracts={existingContracts}
            />
          </TabContainer>}
        {value === 3 &&
          <TabContainer>
            <ContractsList
              newContracts={newContracts}
            />
          </TabContainer>}
        {value === 4 &&
          <TabContainer>
            <EventsList
              events={events}
              totalEvents={totalEvents}
            />
          </TabContainer>}
      </div>
    );
  }
}

SimpleTabs.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SimpleTabs);
