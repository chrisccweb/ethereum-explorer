const blocks = {
  mainnet: {},
  rinkeby: {},
  kovan: {},
  ropsten: {},
};

const tx = {
  mainnet: {},
  rinkeby: {},
  kovan: {},
  ropsten: {},
};

const receipts = {
  mainnet: {},
  rinkeby: {},
  kovan: {},
  ropsten: {},
};

const contracts = {
  mainnet: {},
  rinkeby: {},
  kovan: {},
  ropsten: {},
};

/**
 * 
 * @param {string} id 
 * @param {object} data 
 * @param {string} type 
 * @param {string} network 
 */
const set = (id, data, type, network) => {
  switch(type) {
      case 'blocks':
        blocks[network][id] = data;
        break;
      case 'tx':
        tx[network][id] = data;
        break;
      case 'receipts':
        receipts[network][id] = data;
        break;
      case 'contracts':
        contracts[network][id] = data;
        break;
      default:
  } 
};

/**
 * 
 * @param {string} id 
 * @param {string} type 
 * @param {string} network
 * 
 * @returns {object}
 */
const get = (id, type, network) => {
  let result;

  switch(type) {
      case 'blocks':
        result = blocks[network][id];
        break;
      case 'tx':
        result = tx[network][id];
        break;
      case 'receipts':
        result = receipts[network][id];
        break;
      case 'contracts':
        result = contracts[network][id];
        break;
      default:
  }
  return result;
};

module.exports = {
    set,
    get,
}