const expect = require('chai').expect;
const { 
  getStats,
  disableConsoleLog,
} = require('../src/utils/stats');

// Imported from etherscan.io (Block #6194079)
const expectedResult = require('./data/blockdata.json');

describe('Block Validation (test data courtesy of etherscan.io)', async () => {
  it(`should validate senders, receivers, uncles and total ether for block #${expectedResult.blockHeight}`, async () => {
    disableConsoleLog(true);
    const result = await getStats(expectedResult.blockHeight);
    const tolerance = result.totalEther / expectedResult.totalEther;
    expect(result.blockHeight).to.be.equal(expectedResult.blockHeight);
    expect(tolerance).to.be.at.least(0.9999999999999999);
    expect(result.uncles).to.be.equal(expectedResult.uncles);
    expect(result.senders).to.be.deep.equal(expectedResult.senders);
    expect(result.receivers).to.be.deep.equal(expectedResult.receivers);
    expect(result.contracts.new).to.be.deep.equal(expectedResult.contracts.new);
    expect(result.contracts.existing).to.have.deep.members(expectedResult.contracts.existing);
  })
});