const HDWalletProvider = require('truffle-hdwallet-provider');
const { mnemonic } = process.env;

module.exports = {
  networks: {
    development: {
      host: 'localhost',
      port: 8545,
      network_id: '*'
    },
    production: {
      host: 'localhost',
      port: 8545,
      network_id: '*'
    },
    docker: {
      host: 'ganache-cli',
      port: 8545,
      network_id: '*'
    },
    kovan: {
      provider: () => {
        return new HDWalletProvider(mnemonic, 'https://kovan.infura.io/v3/9d2eb110339a4c6fa4eb5897cfbc9632')
      },
      network_id: '*',
      gas: 4500000,
      gasPrice: 25000000000
    },
    rinkeby: {
      provider: () => {
        new HDWalletProvider(mnemonic, 'https://rinkeby.infura.io/v3/9d2eb110339a4c6fa4eb5897cfbc9632')
      },
      network_id: '*',
      gas: 4500000,
      gasPrice: 25000000000
    },
    mainnet: {
      provider: () => {
        new HDWalletProvider(mnemonic, 'https://mainnet.infura.io/v3/9d2eb110339a4c6fa4eb5897cfbc9632')
      },
      network_id: '*',
      gas: 4500000,
      gasPrice: 25000000000
    },
    ropsten: {
      provider: () => {
        new HDWalletProvider(mnemonic, 'https://ropsten.infura.io/v3/9d2eb110339a4c6fa4eb5897cfbc9632')
      },
      network_id: 3
    }   
  }
}
