# Ethereum Blockchain Explorer (alpha)

This is a simple blockchain explorer that queries the last X (mainnet, kovan, ropsten or rinkeby) blocks and generate statistics of the senders, receivers, contract status and uncles.

## Getting Started

### Prerequisites

- Developed with Node 8.11.3

### Installing

A step by step to get a development environment running

```
# Clone me and install
npm install

# Run dev server
npm start

# Open http://localhost:3000 in your favorite browser
```

A step by step to get a production environment running

```
# Clone me and install
npm install

# Install pushstate server
npm install -g pushstate-server

# Run prod server
pushstate-server build_webpack

# Open http://localhost:9000 in your favorite browser
```

## Running the test

The test uses Mocha/Chai to validate test data manually scraped from Etherscan. 
```
# Clone me and install (if necessary)
npm install

# Run test
npm test
```

## Built With

* [Truffle Box](https://truffleframework.com/boxes/react) - Web framework
* [Infura](https://infura.io) - Ethereum API bridge
* [Mocha](https://mochajs.org/) - Test framework

## Authors

* **Chris Cowell** - *Initial work* - [CC Web Development](https://gitlab.com/chrisccweb)

## License

This project is licensed under the MIT License

## Acknowledgments

* Thanks to all the framework developers otherwise this code was written from scratch.
