const { 
  blockNumber,
  getBlockByNumber,
  getCode,
  getTransactionByHash,
  getTransactionReceipt,
} = require('./get');

const {
  getLogs,
} = require('./post');

module.exports = {
  blockNumber,
  getBlockByNumber,
  getCode,
  getLogs,
  getTransactionByHash,
  getTransactionReceipt,
};
