const {
  blockNumber,
  getBlockByNumber,
  getCode,
  getTransactionByHash,
  getTransactionReceipt,
} = require('./infura/api');

const {
  get,
  set,
} = require('./cache');

const searchThreshold = process.env.SEARCH_THRESHOLD || 1000000;
let blockHeight,
  blockList,
  ledger,
  contracts,
  totalEther,
  totalEvents,
  uncles,
  singleBlockLookup,
  events,
  network,
  consoleLogEnabled = true;

/**
 * Enable or disable console logging 
 * 
 * @param {boolean} status 
 */
const disableConsoleLog = (status) => {
  consoleLogEnabled = !status;
};

/**
 * Conditional console log wrapper 
 * @param {string} message 
 */
const provideStatus = (message) => {
  if (consoleLogEnabled) {
    console.log(message);
  }
};

const doesAddressExistLocally = (address) => {
  const isAddressTaggedAsContract = (contracts.new.indexOf(address) !== -1) || (contracts.existing.indexOf(address) !== -1);
  const isAddressInLedger = ledger.senders[address] || ledger.receivers[address]; 

  return isAddressInLedger || isAddressTaggedAsContract;
};

/**
 * Clear all stats
 * @param {number} historyLength 
 */
const resetStats = async (historyLength) => {
  provideStatus('Resetting stats. Client cache is running')
  if (singleBlockLookup) {
    blockHeight = historyLength;
    provideStatus('Single block lookup mode for entries greater than ' + searchThreshold + '!!!');
  } else {
    try {
      blockHeight = parseInt(await blockNumber(network), 16);
    } catch (error) {
      console.log(error.message);
    }
  }

  blockList = {};
  ledger = {
    senders: {},
    receivers: {},
    totalTransactions: 0,
  };
  contracts = {
    new: [],
    existing: [],
    totalTransactions: 0,
  };
  totalEther = 0;
  totalEvents = 0;
  uncles = 0;
  events = [];
};

/**
 * Read chosen block(s) statistics from Infura
 * 
 * @param {integer} historyLength
 */
const readBlockStats = async (historyLength) => {
  const blockStart = (singleBlockLookup) ? blockHeight : blockHeight - historyLength + 1;

  for (let index = blockHeight; index >= blockStart; index--) {
    const hexAddress = index.toString(16);
    try {
      // Retrieve block info from cache
      blockList[index] = get(hexAddress, 'blocks', network);

      // Check for cache miss
      if (blockList[index] === undefined) {
        // Retrieve block info from API
        const blockInfo = await getBlockByNumber(hexAddress, network);
        set(hexAddress, blockInfo, 'blocks', network);
        blockList[index] = blockInfo;
      }

      ledger.totalTransactions += blockList[index].transactions.length;
      provideStatus('Retrieved block #' + index);
    } catch (error) {
      console.log(error.message); 
    }
  }
};

const checkForEvents = async (address) => {
  let eventInfo = get(address, 'receipts', network);
  if (eventInfo === undefined) {
    eventInfo = await getTransactionReceipt(address, network);
    set(address, eventInfo, 'receipts', network);
  }

  if (eventInfo !== null && eventInfo.logs.length > 0) {
    const topics = eventInfo.logs.map(element => {
      totalEvents++;
      return element.topics;
    });

    const eventItem = {
      blockNumber: eventInfo.blockNumber,
      logs: eventInfo.logs,
      from: eventInfo.from,
      to: eventInfo.to,
      topics,
    };
    events.unshift(eventItem);
  }
}

/**
 * Check if address is an existing or new contract
 *  
 * @param {string} sender
 * @param {string} receiver 
 */
const checkForContract = async (sender, receiver) => {
  if (doesAddressExistLocally() === false) {
    // Check if address is a newly created contract
    if (receiver === null) {
      // Add address to unique collection of new contract addresses
      contracts.new = [...new Set(contracts.new).add(sender)];
      contracts.totalTransactions++;
    } else {
      try {
        let sendAddressCode = get(sender, 'contracts', network);
        if (sendAddressCode === undefined) {
          // Check if send address is an existing contract
          sendAddressCode = await getCode(sender, network);
          set(sender, sendAddressCode, 'contracts', network);
        }

        if (sendAddressCode !== '0x') {
          // Add address to unique collection of existing contract addresses
          contracts.existing = [...new Set(contracts.existing).add(sender)];
          contracts.totalTransactions++;
        }

        let receiveAddressCode = get(receiver, 'contracts', network);
        if (receiveAddressCode === undefined) {
          // Check if receive address is an existing contract
          receiveAddressCode = await getCode(receiver, network);
          set(receiver, receiveAddressCode, 'contracts', network);
        }

        if (receiveAddressCode !== '0x') {
          // Add address to unique collection of existing contract addresses
          contracts.existing = [...new Set(contracts.existing).add(receiver)];
          contracts.totalTransactions++;
        }
      } catch (error) {
        console.log(error.message); 
      }
    }
  }
};

/**
 * Record any detected uncles in the block data
 * 
 * @param {string[]} block 
 */
const checkForUncles = (block) => {
  uncles += block.uncles.length;
}

/**
 * Update total Ether tally
 * 
 * @param {integer|hex} value 
 */
const updateTotalEther = (value) => {
  const ether = parseInt(value, 16);
  totalEther += ether;
};

/**
 * Update ledger with Ether tally for the sender and receiver address
 * 
 * @param {string} sender 
 * @param {string} receiver 
 * @param {integer} ether 
 */
const updateLedger = async (sender, receiver, value) => {
  if( sender !== null && receiver !== null) {
    const ether = parseInt(value, 16);

    // Update ledger for senders
    if (ledger.senders[sender]) {
      ledger.senders[sender] += ether;
    } else {
      ledger.senders[sender] = ether;
    }

    // Update ledger for receivers
    if (ledger.receivers[receiver]) {
      ledger.receivers[receiver] += ether;
    } else {
      ledger.receivers[receiver] = ether;
    }
  }
};

/** 
 * Process all transactions in each block
 */
const processTransactions = async () => {
  provideStatus('Processing ' + ledger.totalTransactions + ' transaction(s). Patience required. :)');
  for (const prop in blockList) {
    if (typeof prop === 'string') {
      const block = blockList[prop];

      // Check for uncles
      checkForUncles(block);

      // Process transaction statistics
      await Promise.all(
        // eslint-disable-next-line no-loop-func
        block.transactions.map(async address => {
          try {
            let transactionInfo = get(address, 'tx', network);
            if (transactionInfo === undefined) {
              transactionInfo = await getTransactionByHash(address, network);
              set(address, transactionInfo, 'tx', network);
            }

            if (transactionInfo !== null) {
              updateTotalEther(transactionInfo.value);
              await checkForEvents(address);
              await checkForContract(transactionInfo.from, transactionInfo.to);
              await updateLedger(transactionInfo.from, transactionInfo.to, transactionInfo.value);
            }
          } catch (error) {
            console.log(error.message); 
          }
        })
      );
    }
  }
};

/** 
 * Package statistics
 */
const packageReport = () => {
  provideStatus('Done!\n');
  const contractTransactionsPercentage = 
    ( 
      Math.round(contracts.totalTransactions / ledger.totalTransactions * 10000) / 100
    ) || 0;
  const report = {
    blockHeight: blockHeight,
    senders: ledger.senders,
    receivers: ledger.receivers,
    totalEther: totalEther,
    contractTransactionsPercentage,
    contracts,
    uncles,
    events,
    totalEvents,
  };

  return report;
}

/**
 * Query specified history or specific block 
 * @param {number} historyLength 
 */
const getStats = async (historyLength, net = 'mainnet') => {
  const startTime = new Date();
  network = net;

  // Check if history request is below threshold otherwise check for a specific block (performance driven option)
  singleBlockLookup = (historyLength > searchThreshold);

  try {
    await resetStats(historyLength);
    await readBlockStats(historyLength);
    await processTransactions();
  } catch (error) {
    console.log(error.message);
  }

  const executionTime = (new Date() - startTime) / 1000;
  console.log(`Request took ${executionTime} sec(s).`);

  return packageReport();
};

module.exports = {
  getStats,
  disableConsoleLog, 
};
