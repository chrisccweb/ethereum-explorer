import React, { Component } from 'react'

class Summary extends Component {
  render() { 
    const {
      blockHeight,
      totalETHTransferred,
      uncles, 
      contractTransactionsPercentage
    } = this.props;

    return (  
      <div>
        <p>Block Height <b>#{blockHeight}</b></p>
        <p>Total ETH transferred: <b>{totalETHTransferred/1000000000000000000} ETH</b></p>
        <p>Uncles Found: <b>{uncles}</b></p>
        <p>Contract Transactions: <b>{contractTransactionsPercentage}%</b></p>
      </div>
    );
  }
}
 
export default Summary;
