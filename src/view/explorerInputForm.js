import React, { Component } from 'react'
import { Button } from '@material-ui/core';

class ExplorerInputForm extends Component {
    render() { 
        const {
            historySize,
            processRequest,
            processing,
            selectedNetwork,
            submitMessage,
            updatedSelectedNetwork,
            updateHistoryRequest,
        } = this.props;

        const disableButton = processing || (historySize <= 0);

        return (
            <div>
                <h3>How many blocks back do you want to explore?</h3>
                <input type="number" onChange={updateHistoryRequest} />
                <select value={selectedNetwork} onChange={updatedSelectedNetwork}>
                    <option value="mainnet">Mainnet</option>
                    <option value="ropsten">Ropsten</option>
                    <option value="rinkeby">Rinkeby</option>
                    <option value="kovan">Kovan</option>
                </select>
                <br />
                <small>{submitMessage}</small>
                <br /><br />
                <Button variant="contained" color="primary" onClick={processRequest} disabled={disableButton}>Submit</Button>
                <br /><br />
            </div>
        );
    }
}
 
export default ExplorerInputForm;