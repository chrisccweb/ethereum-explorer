import React, { Component } from 'react'

class ReceiversList extends Component {
    render() { 
        const {
            receivers,
            newContracts,
            existingContracts,
        } = this.props;

        let count = 0;

        return ( 
            <div>
            <h3>Receivers</h3>
            <table>
                <thead>
                <tr>
                    <th></th>
                    <th>Receivers ({Object.keys(receivers).length} unique)</th>
                    <th>Total Ether</th>
                    <th>Contract?</th>
                </tr>
                </thead>
                <tbody>
                {Object.entries(receivers).map(([address, ether]) => {
                const isContract = newContracts.indexOf(address) > 0 || existingContracts.indexOf(address) > 0;
                return <tr key={count++}>
                        <td>{count}</td>
                        <td>{address}</td>
                        <td>{ether/1000000000000000000}</td>
                        {
                            isContract
                            ? <td key='contract'>YES</td>
                            : <td key='contract'></td>
                        }
                        </tr>;
                })}
                </tbody>
            </table>
            </div>
        );
    }
}
 
export default ReceiversList;