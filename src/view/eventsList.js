import React, { Component } from 'react';
import { decryptedCode } from './eventLabels';

class EventsList extends Component {
    render() { 
        const {
            events,
            totalEvents,
        } = this.props;
        let count = 0;
        return (
            <div>
                <h3>Contract Events ({totalEvents} found)</h3>
                <table>
                    <thead>
                    <tr>
                        <th>Block No.</th>
                        <th>Sender</th>
                        <th>Receiver</th>
                        <th>KECCAK-256</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        events.map((item) => {
                            const topics = item.topics.map(topic => {
                                return topic.map(element => {
                                    const eventCode =  decryptedCode[element] || element;
                                    return eventCode + '\n';
                                });
                            });
                            return <tr key={count++}>
                                    <td>{parseInt(item.blockNumber, 16)}</td>
                                    <td>{item.from}</td>
                                    <td>{item.to}</td>
                                    <td>{topics}</td>
                                </tr>
                        })
                    }
                    </tbody>
                </table>
            </div>
        );
    }
}
 
export default EventsList;