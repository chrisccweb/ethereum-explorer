import React, { Component } from 'react'
import Tabs from './view/tabs'
import { getStats } from './utils/stats'
import ExplorerInputForm from './view/explorerInputForm';

import './css/oswald.css'
import './css/open-sans.css'
import './css/pure-min.css'
import './App.css'

class App extends Component {
  constructor(props) {
    super()

    this.state = {
      web3: null,
      startBlock: 0,
      historySize: 0,
      totalETHTransferred: 0,
      senders: [],
      receivers: [],
      newContracts: [],
      existingContracts: [],
      submitMessage: '',
      blockHeight: '',
      uncles: '',
      processing: false,
      selectedNetwork: 'mainnet',
      contractTransactionsPercentage: '',
      events: [],
      totalEvents: 0,
    }

    this.updateHistoryRequest = this.updateHistoryRequest.bind(this)
    this.updatedSelectedNetwork = this.updatedSelectedNetwork.bind(this)
  }

  updateHistoryRequest = event => {
    this.setState({ historySize: parseInt(event.target.value, 10) })
  }

  updatedSelectedNetwork = event => {
    this.setState({ selectedNetwork: event.target.value })
  }

  processRequest = async () => {
    this.setState({ 
      processing: true,
      totalETHTransferred: 0,
      senders: {},
      receivers: {},
      newContracts: [],
      existingContracts: [],
      blockHeight: '',
      uncles: '',
      events: [],
      contractTransactionsPercentage: '',
      submitMessage: `Processing the last ${this.state.historySize} block(s).`,
    });

    const { 
      senders,
      receivers,
      totalEther,
      contracts,
      blockHeight,
      uncles,
      contractTransactionsPercentage,
      events,
      totalEvents,
    } = await getStats(this.state.historySize, this.state.selectedNetwork);

    this.setState({
      processing: false,
      totalETHTransferred: totalEther,
      senders: senders,
      receivers: receivers,
      submitMessage: '',
      newContracts: contracts.new,
      existingContracts: contracts.existing,
      contractTransactionsPercentage,
      blockHeight,
      uncles,
      events,
      totalEvents,
    });
  }

  render() {
    return (
      <div className="App">
        <main className="container">
          <ExplorerInputForm 
            historySize={this.state.historySize}
            processRequest={this.processRequest}
            processing={this.state.processing}
            selectedNetwork={this.state.selectedNetwork}
            submitMessage={this.state.submitMessage}
            updatedSelectedNetwork={this.updatedSelectedNetwork}
            updateHistoryRequest={this.updateHistoryRequest}
          />
        </main>
        {
          !this.state.processing &&
          <Tabs
            newContracts={this.state.newContracts} 
            receivers={this.state.receivers} 
            senders={this.state.senders} 
            existingContracts={this.state.existingContracts} 
            events={this.state.events} 
            blockHeight={this.state.blockHeight}
            totalETHTransferred={this.state.totalETHTransferred}
            uncles={this.state.uncles}
            contractTransactionsPercentage={this.state.contractTransactionsPercentage}
            totalEvents={this.state.totalEvents}
          />
        }
      </div>
    );
  }
}

export default App
