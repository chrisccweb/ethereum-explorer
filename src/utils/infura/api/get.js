const fetch = require('node-fetch');

const get = async (request) => {
  try {
    const response = await fetch(request);
    const block = await response.json();
    return block.result;
  } catch (error) {
    console.error(error.message);
  }
}

const blockNumber = async (network = 'mainnet') => {
  try {
    return get(`https://api.infura.io/v1/jsonrpc/${network}/eth_blockNumber`);
  } catch (error) {
    console.log(error.message);
  }
};

const getBlockByNumber = async (id, network = 'mainnet') => {
  try {
    return get(`https://api.infura.io/v1/jsonrpc/${network}/eth_getBlockByNumber?params=["0x${id}",false]`);
  } catch (error) {
    console.log(error.message);
  }
};

const getTransactionByHash = async (hash, network = 'mainnet') => {
  try {
    return get(`https://api.infura.io/v1/jsonrpc/${network}/eth_getTransactionByHash?params=["${hash}"]`);
  } catch (error) {
    console.log(error.message);
  }
};

const getCode = async (hash, network = 'mainnet') => {
  try {
    return get(`https://api.infura.io/v1/jsonrpc/${network}/eth_getCode?params=["${hash}","latest"]`);
  } catch (error) {
    console.log(error.message);
  }
};

const getTransactionReceipt = async (hash, network = 'mainnet') => {
  try {
    return get(`https://api.infura.io/v1/jsonrpc/${network}/eth_getTransactionReceipt?params=["${hash}"]`);
  } catch (error) {
    console.log(error.message);
  }
};

module.exports = {
  blockNumber,
  getBlockByNumber,
  getCode,
  getTransactionByHash,
  getTransactionReceipt,
}