import React, { Component } from 'react'

class ContractsList extends Component {
  render() { 
    const { newContracts } = this.props;
    const newContractsFound = newContracts.length > 0;
    let count = 0;
    return ( 
      newContractsFound
      ? <div>
          <h3>New Contract Addresses</h3>
          <table>
            <thead>
              <tr>
                <th></th>
                <th>Created Contracts</th>
              </tr>
            </thead>
            <tbody>
            {
              newContracts.map((address) => {
                return  <tr key={count++}>
                          <td>{count}</td>
                         <td key='address'>{address}</td>
                        </tr>;
              })
            }
            </tbody>
          </table>
        </div>
        : 'No new contracts found.'
    );
  }
}
 
export default ContractsList;
