const post = async (request, body) => {
  try {
    const init = {
      headers: { 
        'Content-Type': 'application/json',
      },
      method: 'POST',
      body: JSON.stringify(body),
    };

    const response = await fetch(request, init);
    const block = await response.json();
    return block.result;
  } catch (error) {
    console.error(error.message);
  }
}

const getLogs = async (topics, network = 'mainnet') => {
  try {
    const body = {
      "jsonrpc": "2.0",
      "method": "eth_getLogs",
      "params": [
        { 
          "topics": [topics]
        },
      ],
      "id": 1
    };

    return post(`https://${network}.infura.io`, body);
  } catch (error) {
    console.log(error.message);
  }
};

module.exports = {
  getLogs,
};
